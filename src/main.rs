pub mod filter;

use filter::{filter_candidates, parse_search_query};
use std::cmp::{max, min};
use std::fs;
use tuikit::prelude::*;

fn main() {
    // let term: Term<()> = Term::with_height(TermHeight::Percent(30)).unwrap();
    let term: Term<()> = Term::with_height(TermHeight::Fixed(11)).unwrap();
    term.clear_on_exit(false).unwrap();
    let mut candiates: Vec<String> = Vec::new();

    let mut search_query = String::from("");
    let mut selection_idx = 0;
    let mut window_top = 0;

    for i in fs::read_dir("/Users/ashton").unwrap() {
        candiates.push(String::from(
            i.unwrap().path().into_os_string().into_string().unwrap(),
        ));
    }

    let _ = term.print(0, 0, "press arrow key to move the text, (q) to quit");
    let _ = term.present();

    while let Ok(ev) = term.poll_event() {
        let _ = term.clear();
        // let (width, height) = term.term_size().unwrap();

        match ev {
            Event::Key(Key::ESC) | Event::Key(Key::Ctrl('g')) | Event::Key(Key::Ctrl('c')) => break,
            Event::Key(Key::Up) => {
                (selection_idx, window_top) = compute_keyup(selection_idx, window_top)
            }
            Event::Key(Key::Down) => {
                (selection_idx, window_top) =
                    compute_keydown(selection_idx, window_top, candiates.len() - 1)
            }
            Event::Key(Key::Char(c)) => {
                search_query.push_str(&c.to_string());
            }
            Event::Key(Key::Backspace) => {
                if search_query.len() > 0 {
                    drop(search_query.pop().unwrap())
                }
            }
            Event::Key(Key::Enter) => {
                // FIXME: clean up window
                term.clear().expect("Terminal didn't clear nicely");
                term.present().expect("term didn't present right");
                drop(term);

                let filter = parse_search_query(&search_query);
                let filtered = filter_candidates(&candiates, filter);
                if filtered.len() > 0 {
                    selection_idx = min(selection_idx, max(1, filtered.len()) - 1);
                    println!("{}", filtered[selection_idx]);
                } else{
                    println!("");
                }

                break
            }
            Event::Key(other) => drop(term.print(1, 50, format!("got key: {:?}", other).as_str())),
            e => drop(term.print(1, 50, format!("Got event: {:?}", e).as_str())),
        }

        let filter = parse_search_query(&search_query);
        let filtered = filter_candidates(&candiates, filter);

        selection_idx = min(selection_idx, max(1, filtered.len()) - 1);
        window_top = min(window_top, max(10, filtered.len()) - 10);

        print_candidates(&term, &filtered, window_top, selection_idx);
        term.print_with_attr(
            0,
            0,
            "> ",
            Attr {
                fg: Color::BLUE,
                ..Attr::default()
            },
        )
        .unwrap();
        let count = candiates.len();
        let _ = term.print(
            0,
            50,
            format!("idx: {selection_idx} wtop: {window_top} len: {count}").as_str(),
        );
        let _ = term.print(0, 2, &search_query);
        let _ = term.set_cursor(0, search_query.len() + 2);
        let _ = term.present();
    }
}

fn compute_keyup(idx: usize, window_top: usize) -> (usize, usize) {
    (
        max(idx, 1) - 1,
        if idx <= window_top + 2 {
            max(window_top, 1) - 1
        } else {
            window_top
        },
    )
}

fn compute_keydown(idx: usize, window_top: usize, list_bot: usize) -> (usize, usize) {
    //  selection_idx = min(selection_idx + 1, candiates.len()),
    (
        min(idx + 1, list_bot),
        if idx >= window_top + 7 {
            min(window_top + 1, list_bot - 8)
        } else {
            window_top
        },
    )
}

fn print_candidates(
    term: &Term<()>,
    candidates: &Vec<&String>,
    window_top: usize,
    selection: usize,
) {
    // term.print_with_attr(row, col, content, attr)
    for idx in window_top..min(candidates.len(), window_top + 10) {
        let attrs = if selection == idx {
            Attr {
                effect: Effect::REVERSE,
                ..Attr::default()
            }
        } else {
            Attr { ..Attr::default() }
        };
        term.print_with_attr(1 + idx - window_top, 0, &candidates[idx], attrs)
            .unwrap();
    }
}
