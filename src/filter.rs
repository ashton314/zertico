pub struct Filter<'a> {
    chunks: Vec<Subterm<'a>>
}

enum Subterm<'a> {
    Initalism(&'a str),          // Must interpret as initialism
    Substr(&'a str),             // Must interpret as substr
    Any(&'a str)                 // Can try any of the above
}

pub fn parse_search_query<'a>(query: &'a str) -> Filter<'a> {
    Filter { chunks: query.split(' ').map(|x| Subterm::Any(x)).collect() }
}

pub fn filter_candidates<'a>(candidates: &'a Vec<String>, filter: Filter) -> Vec<&'a String> {
    candidates.iter().filter(|x| match_filter(x, &filter)).collect()
}

fn match_filter(candidate: &str, filter: &Filter) -> bool {
    for st in filter.chunks.iter() {
        match st {
            Subterm::Any(s) => {
                if candidate.contains(s) {
                    continue;
                } else {
                    return false;
                }
            },
            _ => panic!("Unimplemented")
            // Subterm::Initalism(s) => match_as_initials(candidate, s) && continue
        };
    }
    true
}

fn match_as_initals(candidate: &str, initials: &str) -> bool {
    // algo: pull out starting word characters, then check if `initials' is a substring
    candidate.matches("(?<=\\W)\\w").collect::<Vec<&str>>().concat().contains(initials)
}

fn match_as_substr(candidate: &str, substr: &str) -> bool {
    candidate.contains(substr)
}
